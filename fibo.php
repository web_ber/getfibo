<?php
/**
 * Created by PhpStorm.
 * User: stager
 * Date: 04.07.2018
 * Time: 17:25
 */

getFibo();

function getFibo()
{
    $i = 1;
    $out = [];
    while ($i) {
        $s = (1 + sqrt(5)) / 2;
        $res = round((((pow($s, $i)) - (pow((-$s), (-$i)))) / (2 * $s - 1)), 0);

        if ($res >= 10000)  $i = false;

        $out[] = $res;
        $i++;
    }

    $sum = 0;
    foreach ($out as $k => $obj) {
        if ($k % 2 == 0 && $obj <= 10000) {
            echo '[' . $k . '] => ' . $obj . "\n";
            $sum += $obj;
        }
    }

    echo "Сумма четных элементов последовательности Фибоначчи: " . $sum . "\n";

    return $out;
}